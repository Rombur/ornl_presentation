#! /usr/bin/env python
#----------------------------------------------------------------------------#
# Python code
# Author: Bruno Turcksin
# Date: 2015-08-24 18:35:31.012740
#----------------------------------------------------------------------------#

import pylab
import numpy as np

h_error_dofs = np.array([2.5198, 2.7604, 2.9455, 3.0346, 3.1329, 3.2891, 3.4378,
    3.5460, 3.6173, 3.7264, 3.8832, 4.0216, 4.1262, 4.1993, 4.3024, 4.4558,
    4.5974, 4.7062, 4.7834, 4.8775, 4.9869, 5.1523, 5.2769, 5.3711])

h_error_estimate = np.array([-1.6759, -1.9341, -2.2060, -2.4444, -2.6579,
    -2.8379, -3.0214, -3.2022, -3.3959, -3.5838, -3.7617, -3.9394, -4.1145,
    -4.2959, -4.4762, -4.6500, -4.8254, -5.0013, -5.0636, -5.0911, -5.0968,
    -5.1530, -5.2153, -5.2226])


hp_error_dofs = np.array([2.5198, 2.7101, 2.7987, 3.0204, 3.2060, 3.4565,
    3.6734, 3.8510, 4.1723, 4.2456, 4.4013, 4.6907])

hp_error_estimate = np.array([-1.6759, -2.0501, -2.5168, -3.2312, -3.8656,
    -4.2978, -4.9221, -5.4275, -5.9482, -6.2176, -6.5489, -7.3080])


pylab.figure(1)
pylab.plot(h_error_dofs, h_error_estimate, color='blue', linewidth=4., linestyle='-', 
        marker='D', markersize=10)
pylab.plot(hp_error_dofs, hp_error_estimate, color='red', linewidth=4., linestyle='-', 
        marker='D', markersize=10)
pylab.xlabel('DOFS (log10)', fontsize=20)
pylab.ylabel('Error Estimate (log10)', fontsize=20)
pylab.legend(['h-refinement', 'hp-refinement'])
pylab.xticks(fontsize=15)
pylab.yticks(fontsize=15)
pylab.savefig('h_hp_refinement.png')


#----------------------------------------------------------------------------#

dofs = np.array([2.5198, 2.7101, 2.7987, 3.0204, 3.2060, 3.4565, 3.6734,
    3.8610, 4.1723, 4.2456, 4.4013, 4.6907])

error_estimate = np.array([-1.6759, -2.0501, -2.5168, -3.2312, -3.8656,
    -4.2978, -4.9221, -5.4275, -5.9482, -6.2176, -6.5489, -7.3080])

error = np.array([-2.3140, -2.6444, -3.1297, -4.0175, -4.5987, -4.9294,
    -5.6659, -6.0969, -6.7134, -6.9671, -7.5356, -8.0461])

pylab.figure(2)
pylab.plot(dofs, error_estimate, color='blue', linewidth=4., linestyle='-', 
        marker='D', markersize=10)
pylab.plot(dofs, error, color='red', linewidth=4., linestyle='-', marker='D', 
        markersize=10)
pylab.xlabel('DOFS (log10)', fontsize=20)
pylab.ylabel('Error (log10)', fontsize=20)
pylab.legend(['Error Estimate', 'Error'])
pylab.xticks(fontsize=15)
pylab.yticks(fontsize=15)
pylab.savefig('error_error_estimate.png')
